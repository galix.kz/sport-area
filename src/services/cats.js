import axios from 'axios'


const API_URL = process.env.REACT_APP_API_URL

const getCategories = async () => {
  const data = await axios(`${API_URL}/playcategories/`)
  if (data && data.status === 200) return data

  console.log('error fetching categories list from api')
  return null
}

const getLocations = async () => {
  const data = await axios(`${API_URL}/locations/`)
  if (data && data.status === 200) return data

  console.log('error fetching categories list from api')
  return null
}

const getCategoryDetails = async id => {
  const data = await axios(`${API_URL}/playcategories/${id}/`)
  if (data && data.status === 200) return data

  console.log(`error fetching category${id} details from api`)
  return null
}

export { getCategories, getCategoryDetails, getLocations }
