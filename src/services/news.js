import axios from 'axios'


const API_URL = process.env.REACT_APP_API_URL


const getNews = async () => {
  console.log(`${API_URL}/news/?page=8`)
  fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then(response => response.json())
    .then(json => console.log(json))

  const data = await axios(`${API_URL}/news/?page=8`)
  if (data && data.status === 200) return data

  console.log('error fetching news from api')
  return null
}

export { getNews }
