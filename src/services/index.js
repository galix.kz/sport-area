import { getCategories, getCategoryDetails } from 'services/cats'
import { getNews } from 'services/news'


export { getNews, getCategoryDetails, getCategories }
