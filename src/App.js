import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Header from 'components/common/header/Header'
import Footer from 'components/common/footer/Footer'
import Home from 'layouts/home/Home'
import Register from 'components/Auth/Register'
import { Provider } from 'mobx-react'
import UserStore from './stores/UserStore'


require('dotenv').config()


function App() {
  const stores = {
    userStore : UserStore
  }

  return (
    <BrowserRouter>
      <Provider {...stores}>
        <div>
          <Header/>
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route exact path='/register' component={Register}/>
          </Switch>
          <Footer/>
        </div>
      </Provider>
    </BrowserRouter>
  )
}

export default App
