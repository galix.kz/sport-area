import { computed, observable, action } from 'mobx'
import axios from 'axios'


class UserStore {
  @observable user = {};
  @observable token = '';
  @observable userRegistered = false
  @observable error = []

  constructor() {
    this.token = localStorage.getItem('token')
  }

  @computed get isAuthenticated() {
    return !!this.token
  }

  @action register(data){
    console.log(this)
    console.log(process.env)
    const that = this
    axios.post(`${process.env.REACT_APP_API_URL}/api/auth/users/`, data)
      .then(file=>true).catch(err=>{
        console.log(err.response.data.email)
      if(err.response.data.email){
        const error = that.error.slice()
        error.push('Пользователь с таким email уже существует')
        that.error = error
        console.log(that.error)

      }
      return false
    })
  }
  re
}

export default  new UserStore()
