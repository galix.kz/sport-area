import React, { useEffect, useState } from 'react'
import {withRouter} from 'react-router-dom'
import Slider from 'react-slick'
import { getCategories, getNews } from 'services'
import phoneImg from 'assets/img/phone.png'

import './Home.scss'
import Modal from 'components/common/modal/Modal'
import SoccerImg from 'assets/img/soccer.png'
import BasketballImg from 'assets/img/basketball.png'
import TennisImg from 'assets/img/tennis.jpg'
import PingPongImg from 'assets/img/ping_pong.png'
import VolleyballImg from 'assets/img/volleyball.png'

import { getLocations } from 'services/cats'
import { inject, observer } from 'mobx-react'
import slickSettings from './slickSettings'


let CategoryImg

const setCategoryImg = categoryId => {
  if (categoryId === 1) CategoryImg = SoccerImg
  else if (categoryId === 2) CategoryImg = BasketballImg
  else if (categoryId === 3) CategoryImg = TennisImg
  else if (categoryId === 4) CategoryImg = PingPongImg
  else if (categoryId === 5) CategoryImg = VolleyballImg

  return `url(${CategoryImg})`
}
@inject('userStore')
@observer
class Home extends React.Component{

  // states
  constructor(props){
    super(props)
    this.state = {
      cats: [],
      news: []
    }
  }
  componentDidMount() {
    console.log(this.props)
  }

  render() {
    return (
      <div>
        <section>
          <div className='container'>
            <div className='col-lg-12 mt-5 box-1' style={{ border: '1px solid #064482' }}>
              <h1 className='font-weight-light'>
                <span>ВЫБЕРИТЕ ВИД СПОРТА</span>
              </h1>

              { /*<Modal categoryId = {catId} />*/ }

              { /*{ cats.map(category => (*/ }
              { /*  <a href=''*/ }
              { /*     data-toggle='modal'*/ }
              { /*     data-target='#soccerModal'*/ }
              { /*     onClick={()=> setCatId(category.id)}*/ }
              { /*     key={category.id}>*/ }
              { /*    <div*/ }
              { /*      style={{*/ }
              { /*        backgroundImage: setCategoryImg(category.id),*/ }
              { /*        backgroundRepeat: 'no-repeat',*/ }
              { /*        backgroundSize: 'cover',*/ }
              { /*      }}*/ }
              { /*      className='col-lg-12 box text-white my-2 py-4 text-center'>*/ }
              { /*      <div className='card-body'>*/ }
              { /*        <h1 className='font-weight-bold float-left'>{ category.name }</h1>*/ }
              { /*      </div>*/ }
              { /*    </div>*/ }
              { /*  </a>*/ }
              { /*)) }*/ }
            </div>
          </div>
        </section>
        <section>
          <div className='container mb-5'>
            <div className='col-lg-12 mt-5 box-2' style={{ border: '1px solid #064482' }}>
              <h1 className='font-weight-light'>
                <span>НОВОСТИ</span>
              </h1>
              <Slider {...slickSettings}>
                { /*{ news.map(n => (*/ }
                { /*  <div key={n.id} className='text-center mt-2'>*/ }
                { /*    <div className='news fdb-box pt-5 p-4'>*/ }
                { /*      { n.title }*/ }
                { /*      <p className='lead'>{ n.content }</p>*/ }
                { /*      <p className='align-bottom more mt-5 mb-5'>{ n.author }</p>*/ }
                { /*    </div>*/ }
                { /*  </div>*/ }
                { /*)) }*/ }
              </Slider>
            </div>
          </div>
        </section>
        <section>
          <div className='container mb-5'>
            <div className='col-lg-12 mt-5 box-2' style={{ border: '1px solid #064482' }}>
              <h1 className='font-weight-light'>
                <span>Приложение SportArea.kz</span>
              </h1>
              <div className='row'>
                <div className='col-lg-6'>
                  <h4 className='font-weight-light'>
                    Теперь у вас есть возможность быстро, легко и удобно забронировать спортивные
                    площадки
                  </h4>
                  <ul>
                    <li>— Поиск по базе спортивных площадок Казахстана</li>
                    <li>— Возможность забронировать спортивные площадки в несколько нажатий</li>
                    <li>— Личный кабинет для редактирования и оплаты бронирования</li>
                    <li>— Добавление фото и видео с помощью камеры</li>
                    <li>— Возможность подать объявления бесплатно</li>
                    <li>— Свежие новости и статьи по спортивной тематике</li>
                  </ul>
                </div>
                <div className='col-lg-6 d-flex justify-content-around' id='phone_block'>
                  <img id='phone' alt='phone img' src={phoneImg} />
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }

}

export default withRouter(Home)
