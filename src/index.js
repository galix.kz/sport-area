import React from 'react'
import ReactDOM from 'react-dom'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import './index.scss'

import 'jquery'
import 'bootstrap/dist/js/bootstrap.min'
import 'slick-carousel/slick/slick.min'

import App from './App'


ReactDOM.render(<App />, document.getElementById('root'))

