import React from 'react'
import { ErrorMessage, Field, Form, Formik, withFormik } from 'formik'
import * as Yup from 'yup'
import './Auth.scss'
import Button from 'components/common/Button/Button'
import axios from 'axios'
import { inject, observer } from 'mobx-react'


const CustomErrMsg = ({ children }) => <span className='error'>{ children }</span>

@inject('userStore')
@observer
// eslint-disable-next-line react/prefer-stateless-function
class FormLayout extends React.Component {
  render(){

  return (
    <div className='modal-dialog modal-dialog-centered' role='document'>
      <div className='modal-content'>
        <div className='modal-body'>
          <h5>
            Регистрация
          </h5>
          {this.props.userStore.error}
          <Formik  validationSchema={Yup.object().shape({
                                    username: Yup.string().required('Логин отсутствует'),
                                    password: Yup.string().required('Пароль отсутствует'),
                                    first_name: Yup.string().required('Поле Имя пустое'),
                                    last_name: Yup.string().required('Поле Фамилия пустое'),
                                    email: Yup.string().email('Неправильный формат для Эл. почты')
                                      .required('Эл. почта отсутствует'),
                                    phone: Yup.string().required('Телефон отсутствует'),
                                    organization: Yup.string().required('Организация отсутствует'),
                                  })}
                   onSubmit={
                     (values, actions) => {
                       // eslint-disable-next-line max-len
                       if(this.props.userStore.register({...values, profile:
                           {organization: values.organization, phone: values.phone}
                       }))
                         actions.setSubmitting(false)
                       else actions.setSubmitting(false)
                     }
                   }>
            { ({ handleSubmit, handleChange, values, errors, isSubmitting, touched }) => (
              <Form className='text-center'>
                <div className='form-group'>
                  <Field
                    name='username'
                    type='text'
                    className='form-control'
                    id='inputUsernme'
                    placeholder='Имя пользователя'
                  />
                  <ErrorMessage name='username' component={CustomErrMsg}/>
                </div>
                <div className='form-group'>
                  <Field
                    name='password'
                    type='password'
                    className='form-control'
                    id='inputPassword'
                    placeholder='Пароль'
                  />
                  <ErrorMessage name='password' component={CustomErrMsg}/>
                </div>
                <div className='form-group'>
                  <Field
                    name='first_name'
                    type='text'
                    className='form-control'
                    id='inputFirstname'
                    placeholder='Имя'
                  />
                  <ErrorMessage name='first_name' component={CustomErrMsg}/>
                </div>
                <div className='form-group'>
                  <Field
                    name='last_name'
                    type='text'
                    className='form-control'
                    id='inputLastname'
                    placeholder='Фамилия'
                  />
                  <ErrorMessage name='last_name' component={CustomErrMsg}/>
                </div>
                <div className='form-group'>
                  <Field
                    name='email'
                    type='email'
                    className='form-control'
                    id='inputEmail'
                    placeholder='E-mail'
                  />
                  <ErrorMessage name='email' component={CustomErrMsg}/>
                </div>
                <div className='form-group'>
                  <Field
                    name='phone'
                    type='phone'
                    className='form-control'
                    id='inputPhone'
                    placeholder='Номер телефона'
                  />
                  <ErrorMessage name='phone' component={CustomErrMsg}/>
                </div>
                <div className='form-group'>
                  <Field
                    name='organization'
                    type='text'
                    className='form-control'
                    id='inputOrganization'
                    placeholder='Организация'
                  />
                  <ErrorMessage name='organization' component={CustomErrMsg}/>
                </div>
                <div className='form-group'>
                  <Field id='selectedRole' type='hidden' name='isProvider'/>
                </div>
                <Button
                  type='submit'
                  className='btn btn-primary'
                  disabled={
                    isSubmitting ||
                    !!(errors.username && touched.username) ||
                    !!(errors.password && touched.password) ||
                    !!(errors.first_name && touched.first_name) ||
                    !!(errors.last_name && touched.last_name) ||
                    !!(errors.email && touched.email) ||
                    !!(errors.phone && touched.phone) ||
                    !!(errors.organization && touched.organization)
                  }>
                  Зарегистрироваться
                </Button>
              </Form>
            ) }
          </Formik>
        </div>
      </div>
    </div>
  )
  }
}


const RegisterForm = withFormik({

  mapPropsToValues: props => ({
    username: '',
    password: '',
    first_name: '',
    last_name: '',
    email: '',
    phone: '',
    organization: '',
  }),



  handleSubmit: (values, { setSubmitting, props }) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/auth/users/`, {...values, profile:
        {organization: values.organization, phone: values.phone}
    }).then(file=>{
      console.log(file)
    }).catch(err=>{
      console.log(err.response)
    })
  },
})(FormLayout)


export default FormLayout
