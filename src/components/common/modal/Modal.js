import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { getCategoryDetails } from 'services'
import './Modal.scss'



const Modal = ({ categoryId }) => {
  const [title, setTitle] = useState('')
  const [backgroundImg, setBackgroundImg] = useState(null)


  const setModalTitle = id => {
    if (id === 1) setTitle('Футбольное поле')
    else if (id === 2) setTitle('Баскетбольное поле')
    else if (id === 3) setTitle('Теннисный корт')
    else if (id === 4) setTitle('Пинг понг')
    else if (id === 5) setTitle('Волейбольное поле')
  }

  useEffect(() => {
    if (categoryId != null)
      (async function() {
        const details = await getCategoryDetails(categoryId)
        if (details && details.status === 200) {
          setModalTitle(details.data.id)
          setBackgroundImg(details.data.photo)
          console.log('test', details)
        }
      })()
  }, [categoryId])

  return (
    <div
      className='modal fade'
      id='soccerModal'
      tabIndex={-1}
      role='dialog'
      aria-labelledby='exampleModalLabel'
      aria-hidden='true'
    >
      <div className='modal-dialog modal-xl modal-dialog-centered' role='document'>
        <div className='modal-content'>
          <div className='modal-body'
               style={{
                 backgroundImage: `url(${backgroundImg})`,
                 backgroundRepeat: 'no-repeat',
                 backgroundSize: 'cover',
               }}
          >
            <div className='container mb-5'>
              <div className='col-lg-12 mt-5 box-2' style={{ border: '1px solid #064482' }}>
                <h1 className='font-weight-light'>
                  <span>Всего 24 объекта</span>
                </h1>
                <div className='row mt-5 align-items-center'>
                  <div
                    className='col-12 col-sm-10 col-md-8
                               m-auto col-lg-5 box-3 text-right'
                  >
                    <div className='fdb-box fdb-touch p-5 rounded'>
                      <h2 className='modal-title'>{ title }</h2>
                      <p className='lead'>
                        <em>от 10000тг/час.</em>
                      </p>
                      <ul className='text-left pl-3 mt-5 mb-5'>
                        <li>Адрес: г. Алматы, ул. Гагарина, 236</li>
                        <li>Тип: Крытое</li>
                        <li>Размеры: 57х108м</li>
                      </ul>
                      <p className='text-left'>
                        <a href className='btn btn-primary'>
                          Подробнее
                        </a>
                      </p>
                    </div>
                  </div>
                  <div
                    className='col-12 col-sm-10 col-md-8
                               m-auto col-lg-5 box-3 text-right'
                  >
                    <div className='fdb-box fdb-touch p-5 rounded'>
                      <h2 className='modal-title'>{ title }</h2>
                      <p className='lead'>
                        <em>от 10000тг/час.</em>
                      </p>
                      <ul className='text-left pl-3 mt-5 mb-5'>
                        <li>Адрес: г. Алматы, ул. Гагарина, 236</li>
                        <li>Тип: Крытое</li>
                        <li>Размеры: 57х108м</li>
                      </ul>
                      <p className='text-left'>
                        <a href className='btn btn-primary'>
                          Подробнее
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='modal-footer'>
            <button type='button' className='btn btn-secondary' data-dismiss='modal'>
              ЗАКРЫТЬ
            </button>
            <button type='button' className='btn btn-primary'>
              ПОСМОТРЕТЬ ЕЩЕ
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

Modal.propTypes = {
  categoryId: PropTypes.number,
}

export default Modal
