import React from 'react'
import './Button.scss'


const Button = ({ children, className, disabled }) => {
  let btnClassname = ''
  if(disabled)
    btnClassname = 'btn-disabled'
  else
    btnClassname = ''

  return <button
    type='submit'
    disabled={disabled}
    className={`${className  } ${ btnClassname}`}>
    { children }
  </button>
}

export default Button

