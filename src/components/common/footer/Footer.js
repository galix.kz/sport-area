import React from 'react'


const Footer = () => (
  <footer className='fdb-block footer-large blue'>
    <div className='container'>
      <div className='row align-items-top text-center pt-5'>
        <div className='col-12 col-sm-6 col-md-4 col-lg-3 text-sm-left'>
          <h6>Для партнеров</h6>
          <nav className='nav flex-column'>
            <a className='nav-link' href>
                Номер телефона
            </a>
            <a className='nav-link' href>
                E-mail
            </a>
            <a className='nav-link' href>
                Адрес офиса
            </a>
          </nav>
        </div>
        <div className='col-12 col-sm-6 col-md-4 col-lg-3 mt-5 mt-sm-0 text-sm-left'>
          <h6>Информация</h6>
          <nav className='nav flex-column'>
            <a className='nav-link' href>
                Контактные данные
            </a>
            <a className='nav-link' href>
                Телефон
            </a>
            <a className='nav-link' href>
                Адрес
            </a>
            <a className='nav-link' href>
                E-mail
            </a>
          </nav>
        </div>
        <div className='col-12 col-md-4 col-lg-3 text-md-left mt-5 mt-md-0'>
          <h6>Дополнительные сервисы</h6>
          <a className='nav-link' href>
              Мобильная версия сайта
          </a>
        </div>
        <div className='col-12 col-lg-2 ml-auto text-lg-left mt-4 mt-lg-0'>
          <h6>Мы в социальных сетях</h6>
          <p className='lead'>
            <a href className='mx-2'>
              <i className='fab fa-twitter' aria-hidden='true' />
            </a>
            <a href className='mx-2'>
              <i className='fab fa-facebook' aria-hidden='true' />
            </a>
            <a href className='mx-2'>
              <i className='fab fa-instagram' aria-hidden='true' />
            </a>
          </p>
        </div>
      </div>
      <div className='row mt-3 pb-5'>
        <div className='col text-center'>2019 все права защищены ИП "MultiDigital"</div>
      </div>
    </div>
  </footer>
  )

export default Footer
