import React from 'react'
import './Header.scss'
import logoImg from 'assets/img/logo.png'
import { Link, withRouter } from 'react-router-dom'
import RegisterForm from '../../Auth/Register'


const Header = ({history}) => (
  <nav className='navbar navbar-expand-lg navbar-dark'>
    <img className='mr-2' src={logoImg} alt='logo img' height={50}/>

    <div className='collapse navbar-collapse' id='navbar1'>
      <ul className='navbar-nav mr-auto'>
        <li className='nav-item'>
          <a className='navbar-brand font-weight-bold' href='/'>
            ОНЛАЙН БРОНИРОВАНИЕ
            <br/>
            СПОРТИВНЫХ ПЛОЩАДОК
          </a>
        </li>
      </ul>
      <form className='form-inline my-2 my-lg-0'>
        <div className='input-group'>
          <input
            type='text'
            className='form-control'
            placeholder='Поиск...'
            aria-describedby='basic-addon2'
          />
          <div className='input-group-append'>
            <button className='btn btn-light btn-outline-secondary' type='button'>
              <i className='fas fa-search'/>
            </button>
          </div>
        </div>
      </form>
      <ul className='navbar-nav ml-3'>
        <li className='nav-item'>
          <button
            type='button'
            className='btn btn-white'
            data-toggle='modal'
            data-target='#loginModal'
          >
            ВОЙТИ
            { ' ' }
            <i className='fas fa-user'/>
          </button>
        </li>
      </ul>
      { /* Login Modal */ }
      <div
        className='modal fade'
        id='loginModal'
        tabIndex={-1}
        role='dialog'
        aria-labelledby='loginModalTitle'
        aria-hidden='true'
      >
        <div className='modal-dialog modal-dialog-centered' role='document'>
          <div className='modal-content'>
            <div className='modal-body'>
              <h5 className='modal-title mb-3 text-center blue-text' id='loginModalLongTitle'>
                Вход в личный кабинет
              </h5>
              <form className='text-center'>
                <div className='form-group'>
                  <input
                    type='login'
                    className='form-control'
                    id='exampleInputLogin1'
                    aria-describedby='loginHelp'
                    placeholder='Логин'
                  />
                </div>
                <div className='form-group'>
                  <input
                    type='password'
                    className='form-control'
                    id='exampleInputPassword1'
                    placeholder='Пароль'
                  />
                  <a
                    style={{ cursor: 'pointer' }}
                    data-toggle='modal'
                    data-target='#PassModal'
                    data-dismiss='modal'
                    aria-label='Close'
                  >
                    <small>Забыли пароль?</small>
                  </a>
                </div>
                <button type='submit' className='btn btn-primary modal-button'>
                  Войти
                </button>
                <button
                  // data-toggle='modal'
                  // data-target='#isProvider'
                  data-toggle='modal'
                  data-target='#registerModal'
                  data-dismiss='modal'
                  aria-label='Close'
                  className='btn btn-primary modal-button'
                >
                  Регистрация
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
      { /* End Login Modal */ }
      { /* Reset Pass Modal */ }
      <div
        className='modal fade'
        id='PassModal'
        tabIndex={-1}
        role='dialog'
        aria-labelledby='loginModalTitle'
        aria-hidden='true'
      >
        <div className='modal-dialog modal-dialog-centered' role='document'>
          <div className='modal-content'>
            <div className='modal-body'>
              <h5 className='modal-title mb-3 text-center blue-text' id='loginModalLongTitle'>
                Восстановление пароля
              </h5>
              <form className='text-center'>
                <div className='form-group'>
                  <input
                    type='email'
                    className='form-control'
                    id='exampleInputLogin1'
                    aria-describedby='emailHelp'
                    placeholder='Введите E-mail'
                  />
                </div>
                <button type='submit' className='btn btn-primary'>
                  Далее
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
      { /* End Reset Pass Modal */ }
      { /* Register Modal */ }
      <div
        className='modal fade'
        id='isProvider'
        tabIndex={-1}
        role='dialog'
        aria-labelledby='loginModalTitle'
        aria-hidden='true'
      >
        <div className='modal-dialog modal-dialog-centered' role='document'>
          <div className='modal-content'>
            <div className='modal-body'>
              <h5 className='modal-title mb-3 text-center blue-text' id='loginModalLongTitle'>
                Выберите роль
              </h5>
              <form action="{{ asset('/') }}" id='selectModal' className='text-center'>
                <div className='form-group'>
                  <select name='is_provider' type='text' className='form-control' id='selectRole'>
                    <option disabled selected hidden>
                      Выберите роль
                    </option>
                    <option type='checkbox' value='true'>
                      Собственник
                    </option>
                    <option value='false'>Обычный смертный</option>
                  </select>
                </div>
                <button type='button' id='send'>
                  заполнить
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
      { /* End Register Modal */ }
      { /* Register Modal */ }
      { /*

      <div
        className='modal fade'
        id='registerModal'
        tabIndex={-1}
        role='dialog'
        aria-labelledby='loginModalTitle'
        aria-hidden='true'
      >
        <div className='modal-dialog modal-dialog-centered' role='document'>
          <div className='modal-content'>
            <div className='modal-body'>
              <h5 className='modal-title mb-3 text-center blue-text' id='loginModalLongTitle'>
                Регистрация
              </h5>
              <form action method='POST' className='text-center'>
                @csrf
                <div className='form-group'>
                  <input
                    name='username'
                    type='text'
                    className='form-control'
                    id='inputUsernme'
                    placeholder='Имя пользователя'
                  />
                </div>
                <div className='form-group'>
                  <input
                    name='password'
                    type='password'
                    className='form-control'
                    id='inputPassword'
                    placeholder='Пароль'
                  />
                </div>
                <div className='form-group'>
                  <input
                    name='first_name'
                    type='text'
                    className='form-control'
                    id='inputFirstname'
                    placeholder='Имя'
                  />
                </div>
                <div className='form-group'>
                  <input
                    name='last_name'
                    type='text'
                    className='form-control'
                    id='inputLastname'
                    placeholder='Фамилия'
                  />
                </div>
                <div className='form-group'>
                  <input
                    name='email'
                    type='email'
                    className='form-control'
                    id='inputEmail'
                    placeholder='E-mail'
                  />
                </div>
                <div className='form-group'>
                  <input
                    name='phone'
                    type='phone'
                    className='form-control'
                    id='inputPhone'
                    placeholder='Номер телефона'
                  />
                </div>
                <div className='form-group'>
                  <input
                    name='organization'
                    type='text'
                    className='form-control'
                    id='inputOrganization'
                    placeholder='Организация'
                  />
                </div>
                <div className='form-group'>
                  <input id='selectedRole' type='hidden' name='isProvider'/>
                </div>
                <button type='submit' className='btn btn-primary'>
                  Зарегистрироваться
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
      { /* End Register Modal
      */ }
      <div
        className='modal fade'
        id='registerModal'
        tabIndex={-1}
        role='dialog'
        aria-labelledby='loginModalTitle'
        aria-hidden='true'
      >
        <RegisterForm/>
      </div>
    </div>
  </nav>
)

export default withRouter(Header)
